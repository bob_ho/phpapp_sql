SQL Queries for phpapp

# Docker Instructions

## Pull mysql image
docker pull mysql

## Run docker image, with environment variables
docker run --name mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_USER=admin -e MYSQL_PASSWORD=@dminPass123 -e MYSQL_DATABASE=phpapp -p 3306:3306 -t mysql

## import database dump into database
cat phpapp.sql | docker exec -i mysql /usr/bin/mysql -u admin --password=@dminPass123 phpapp